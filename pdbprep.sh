#!/bin/bash
for dir in */; do
   echo "$dir"
   cd $dir
   eval "rm *prepared*"
   ## for rec
   rec_name=$(find . -name "rec*" | sed -E 's|^\.{1,2}/||')
   rec_name_no_extension=${rec_name%.pdb} 
   echo $rec_name_no_extension
   eval "pdbprep --no-minimize $rec_name"
   mv prepared.pdb $rec_name_no_extension"_prepared.pdb"
   mv prepared.psf $rec_name_no_extension"_prepared.psf"

   # for lig
   lig_name=$(find . -name "lig*" | sed -E 's|^\.{1,2}/||')
   lig_name_no_extension=${lig_name%.pdb} 
   echo $lig_name_no_extension
   eval "pdbprep --no-minimize $lig_name"
   mv prepared.pdb $lig_name_no_extension"_prepared.pdb"
   mv prepared.psf $lig_name_no_extension"_prepared.psf"

   cd ../
done
